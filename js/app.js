var dataArray = [
  {
    'name': 'Tobby',
    'image': 'img/cat1.jpg',
    'clickCount': 0,
    'nickNames': ['nickname1', 'nickname2', 'nickname3']
  },
  {
    'name': 'Twitchy',
    'image': 'img/cat2.jpg',
    'clickCount': 0,
    'nickNames': ['nickname4', 'nickname5', 'nickname6']
  },
  {
    'name': 'Micky',
    'image': 'img/cat3.jpg',
    'clickCount': 0,
    'nickNames': ['nickname7', 'nickname8', 'nickname9']
  }
];

var Cat = function(data) {
  this.name = ko.observable(data.name);
  this.imgSrc = ko.observable(data.image);
  this.clickCount = ko.observable(data.clickCount);
  this.nicknames = ko.observableArray(data.nickNames);

  this.title = ko.computed(function() {
    var result = '';
    var c = this.clickCount();

    if (c < 10) {
      result = 'newborn';
    } else if (c < 20) {
      result = 'infant';
    } else if (c < 50) {
      result = 'teenager';
    } else if (c < 100) {
      result = 'adult';
    } else {
      result = 'ninja';
    }

    return result;
  }, this);
};

var ViewModel = function() {
  var self = this;
  self.catArray = [];

  for (var i = 0; i < dataArray.length; i++) {
    self.catArray.push(new Cat(dataArray[i]));
  }

  self.currentCat = ko.observable(self.catArray[0]);

  self.incrementClickCount = function() {
    var c = this.clickCount();
    this.clickCount(c + 1);
  };

  self.updateCurrentCat = function() {
    self.currentCat(ko.observable(this));
  };
};

ko.applyBindings(new ViewModel());
